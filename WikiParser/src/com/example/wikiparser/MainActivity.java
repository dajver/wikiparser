package com.example.wikiparser;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MainActivity extends Activity {

	private static final String FIRST = "data";
	private static ArrayList<HashMap<String, Object>> myBooks;
	public ListView listView;
	private SimpleAdapter adapter;
	private String data;

	/** @param result */
	public void JSONURL(String result) {

		try {
			JSONObject json = new JSONObject(result);
			JSONObject urls = json.getJSONObject("parse");
			JSONArray links = urls.getJSONArray("links");
			for (int i = 0; i < links.length(); i++) {
				HashMap<String, Object> hm;
				hm = new HashMap<String, Object>();
				hm.put(FIRST, links.getJSONObject(i).getString("*").toString());
				myBooks.add(hm);
				SimpleAdapter adapter = new SimpleAdapter(this, myBooks, R.layout.list,
						new String[] { FIRST, }, new int[] { R.id.textView1 });
				listView.setAdapter(adapter);
				listView.setChoiceMode(AbsListView.CHOICE_MODE_SINGLE);
			}
		} catch (JSONException e) {
			Log.e("log_tag", "Error parsing data " + e.toString());
		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		//
		listView = (ListView) findViewById(R.id.listView1);
		myBooks = new ArrayList<HashMap<String, Object>>();
		new LongOperation()
				.execute("http://ru.wikipedia.org/w/api.php?format=json&action=parse&page=%D0%A1%D0%BF%D0%B8%D1%81%D0%BE%D0%BA_%D0%B3%D0%BE%D1%81%D1%83%D0%B4%D0%B0%D1%80%D1%81%D1%82%D0%B2");
		listView.setClickable(true);
		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

				String str = parent.getAdapter().getItem(position).toString();
				Pattern p = Pattern.compile("\\{data=([^}]+)\\}");
				Matcher str1 = p.matcher(str);
				while (str1.find()) {
					String data = str1.group(1).replace(" ", "_");
					Intent intent = new Intent(MainActivity.this, SecondActivity.class);
					intent.putExtra(SecondActivity.TEXT, data);
					startActivity(intent);
				}
			}
		});
	}

	private class LongOperation extends AsyncTask<String, Void, String> {

		String responseString = null;

		public LongOperation() {

			// TODO
		}

		@Override
		protected String doInBackground(String... params) {

			HttpClient httpclient = new DefaultHttpClient();
			HttpResponse response;
			try {
				response = httpclient.execute(new HttpGet(params[0]));
				StatusLine statusLine = response.getStatusLine();
				if (statusLine.getStatusCode() == HttpStatus.SC_OK) {
					ByteArrayOutputStream out = new ByteArrayOutputStream();
					response.getEntity().writeTo(out);
					out.close();
					responseString = out.toString();
				} else {
					response.getEntity().getContent().close();
					throw new IOException(statusLine.getReasonPhrase());
				}
			} catch (ClientProtocolException e) {
				Log.d("debug:", "failed: " + e.getMessage());
			} catch (IOException e) {
				Log.d("debug:", "failed: " + e.getMessage());
			}
			return responseString;
		}

		@Override
		protected void onPostExecute(String result) {

			JSONURL(responseString);
		}
	}
}
