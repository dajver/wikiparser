package com.example.wikiparser;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SecondActivity extends Activity {

	public static final String TEXT = "Text";
	private static final String FIRST = "data";
	private static ArrayList<HashMap<String, Object>> myBooks;
	private static final String SECOND = "section";
	public ListView listView;
	private SimpleAdapter adapter;
	private String data;

	/** @param result */
	public void JSONURL(String result) {

		try {
			JSONObject json = new JSONObject(result);
			JSONObject urls = json.getJSONObject("parse");
			JSONArray links = urls.getJSONArray("sections");
			for (int i = 0; i < links.length(); i++) {
				HashMap<String, Object> hm;
				hm = new HashMap<String, Object>();
				hm.put(FIRST, links.getJSONObject(i).getString("line").toString());
				hm.put(SECOND, links.getJSONObject(i).getString("index").toString());
				myBooks.add(hm);
				SimpleAdapter adapter = new SimpleAdapter(this, myBooks, R.layout.list,
						new String[] { FIRST, }, new int[] { R.id.textView1 });
				listView.setAdapter(adapter);
				listView.setChoiceMode(AbsListView.CHOICE_MODE_SINGLE);
			}
		} catch (JSONException e) {
			Log.e("log_tag", "Error parsing data " + e.toString());
		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		//
		listView = (ListView) findViewById(R.id.listView1);
		myBooks = new ArrayList<HashMap<String, Object>>();
		final Bundle extras = getIntent().getExtras();
		new LongOperation().execute("http://ru.wikipedia.org/w/api.php?format=json&action=parse&page="
				+ extras.getString(TEXT) + "&prop=sections");
		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

				String str = parent.getAdapter().getItem(position).toString();
				Pattern p = Pattern.compile("\\{data=([^}]+), section=([^}]+)\\}");
				Matcher str1 = p.matcher(str);
				while (str1.find()) {
					String data1 = str1.group(1).replace(" ", "_");
					String data2 = str1.group(2);
					Intent intent = new Intent(SecondActivity.this, ThirdActivity.class);
					intent.putExtra(ThirdActivity.TEXT, extras.getString(TEXT));
					intent.putExtra(ThirdActivity.SECTION, data2);
					startActivity(intent);
				}
			}
		});
	}

	private class LongOperation extends AsyncTask<String, Void, String> {

		String responseString = null;

		public LongOperation() {

			// TODO
		}

		@Override
		protected String doInBackground(String... params) {

			HttpClient httpclient = new DefaultHttpClient();
			HttpResponse response;
			try {
				response = httpclient.execute(new HttpGet(params[0]));
				StatusLine statusLine = response.getStatusLine();
				if (statusLine.getStatusCode() == HttpStatus.SC_OK) {
					ByteArrayOutputStream out = new ByteArrayOutputStream();
					response.getEntity().writeTo(out);
					out.close();
					responseString = out.toString();
				} else {
					response.getEntity().getContent().close();
					throw new IOException(statusLine.getReasonPhrase());
				}
			} catch (ClientProtocolException e) {
				Log.d("debug:", "failed: " + e.getMessage());
			} catch (IOException e) {
				Log.d("debug:", "failed: " + e.getMessage());
			}
			return responseString;
		}

		@Override
		protected void onPostExecute(String result) {

			JSONURL(responseString);
		}
	}
}