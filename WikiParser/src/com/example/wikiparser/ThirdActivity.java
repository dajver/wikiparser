package com.example.wikiparser;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.webkit.WebView;
import android.widget.SimpleAdapter;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

public class ThirdActivity extends Activity {

	public static final String SECTION = "Section";
	public static final String TEXT = "Text";
	private static final String FIRST = "data";
	private static ArrayList<HashMap<String, Object>> myBooks;
	public WebView web;
	private SimpleAdapter adapter;
	private String data;

	/** @param result */
	public void JSONURL(String result) {

		try {
			// создали читателя json объектов и отдали ему строку - result
			JSONObject json = new JSONObject(result);
			// дальше находим вход в наш json им является ключевое слово data
			JSONObject urls = json.getJSONObject("parse");
			JSONObject data = urls.getJSONObject("text");
			web.loadData(data.getString("*").toString(), "text/html; charset=UTF-8", null);
		} catch (JSONException e) {
			Log.e("log_tag", "Error parsing data " + e.toString());
		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.second_activity);
		//
		web = (WebView) findViewById(R.id.webView1);
		myBooks = new ArrayList<HashMap<String, Object>>();
		Bundle extras = getIntent().getExtras();
		Log.d("", "http://ru.wikipedia.org/w/api.php?format=json&action=parse&page=" + extras.getString(TEXT)
				+ "&prop=text&section=" + extras.getString(SECTION));
		new LongOperation().execute("http://ru.wikipedia.org/w/api.php?format=json&action=parse&page="
				+ extras.getString(TEXT) + "&prop=text&section=" + extras.getString(SECTION));
	}

	private class LongOperation extends AsyncTask<String, Void, String> {

		String responseString = null;

		public LongOperation() {

			// TODO
		}

		@Override
		protected String doInBackground(String... params) {

			HttpClient httpclient = new DefaultHttpClient();
			HttpResponse response;
			try {
				response = httpclient.execute(new HttpGet(params[0]));
				StatusLine statusLine = response.getStatusLine();
				if (statusLine.getStatusCode() == HttpStatus.SC_OK) {
					ByteArrayOutputStream out = new ByteArrayOutputStream();
					response.getEntity().writeTo(out);
					out.close();
					responseString = out.toString();
				} else {
					response.getEntity().getContent().close();
					throw new IOException(statusLine.getReasonPhrase());
				}
			} catch (ClientProtocolException e) {
				Log.d("debug:", "failed: " + e.getMessage());
			} catch (IOException e) {
				Log.d("debug:", "failed: " + e.getMessage());
			}
			return responseString;
		}

		@Override
		protected void onPostExecute(String result) {

			JSONURL(responseString);
		}
	}
}